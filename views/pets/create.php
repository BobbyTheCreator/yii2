<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
?>
<?php $form = ActiveForm::begin([
    'id' => 'create',
    'enableAjaxValidation' => true,
    'validationUrl' => Yii::$app->urlManager->createUrl('pets/ajax')
]); ?>

    <?= $form->field($model, 'name') ?>
    <?= $form->field($model, 'ownerEmail') ?>
    <?= $form->field($model, 'owner') ?>
    <?= $form->field($model, 'species') ?>
    <?= $form->field($model, 'sex') ?>
    <?php echo $form->field($model,'birth')->
    widget(DatePicker::className(),[
        'name' => 'birth',
        'value' => date('d-M-Y', strtotime('+2 days')),
        'options' => ['placeholder' => 'Select birth date ...'],
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true
        ]
    ]) ?>
    <?php echo $form->field($model,'death')->
    widget(DatePicker::className(),[
        'name' => 'death',
        'value' => date('d-M-Y', strtotime('+2 days')),
        'options' => ['placeholder' => 'Select death date ...'],
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true
        ]
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>