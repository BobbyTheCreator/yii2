<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
?>
<h1>Pets</h1>
<ul>
<?php foreach ($pets as $pet): ?>
    <li>
        <?= Html::encode("{$pet->pUi} ({$pet->name}) ({$pet->ownerEmail}) ({$pet->owner})({$pet->species}) ({$pet->sex}) ({$pet->birth})") ?>
        <?= $pet->death ?>
    </li>
<?php endforeach; ?>
</ul>

<?= LinkPager::widget(['pagination' => $pagination]) ?>