<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UserInfo */
/* @var $form ActiveForm */
?>
<div class="user-input-createUser">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'uiFullName') ?>
        <?= $form->field($model, 'uiBirth') ?>
        <?= $form->field($model, 'uiHobby') ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- user-input-createUser -->
