<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_info".
 *
 * @property int $uiUin
 * @property string $uiFullName
 * @property string $uiBirth
 * @property string $uiHobby
 * @property string $uiIsActive
 */
class UserInfo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_info';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uiFullName', 'uiBirth', 'uiHobby'], 'required'],
            [['uiBirth'], 'safe'],
            [['uiFullName', 'uiHobby'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'uiUin' => 'Uin',
            'uiFullName' => 'Full Name',
            'uiBirth' => 'Birth',
            'uiHobby' => 'Hobby',
            'uiIsActive' => 'IsActive',
        ];
    }
}
