<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "pets".
 *
 * @property integer $pUi
 * @property string $name
 * @property string $ownerEmail
 * @property string $owner
 * @property string $species
 * @property string $sex
 * @property date $birth
 * @property date $date
*/
class Pets extends ActiveRecord
{
    const STATUS_REQUEST =0;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pets';
    }

    public function rules()
    {
        return [
            [['name', 'ownerEmail', 'owner', 'species', 'sex', 'birth'], 'required'],
            ['death', 'safe'],
            ['ownerEmail', 'email'],
            ['ownerEmail', 'unique', 'message' => 'Этот email уже зарегистрирован.'],
        ];
    }
}