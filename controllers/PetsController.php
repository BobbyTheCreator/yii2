<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\data\Pagination;
use app\models\Pets;
use yii\web\Response;
use yii\widgets\ActiveForm;

class PetsController extends Controller
{
    public function actionIndex()
    {
        $query = Pets::find();

        $pagination = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $query->count(),
        ]);

        $pets = $query->orderBy('pUi')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        return $this->render('index', [
            'pets' => $pets,
            'pagination' => $pagination,
        ]);
    }

    public function actionAjax()
    {
        $model = new Pets();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        } else {
            // либо страница отображается первый раз, либо есть ошибка в данных
            return $this->render('create', ['model' => $model]);
        }
    }

    public function actionCreate()
    {
        $model = new Pets();

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->save()) {
            // данные в $model удачно проверены

            // делаем что-то полезное с $model ...
 
            return $this->redirect(['index']);
        } else {
            // либо страница отображается первый раз, либо есть ошибка в данных
            return $this->render('create', ['model' => $model]);
        }
    }
}